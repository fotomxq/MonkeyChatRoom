<?php

/**
 * 聊天室首页
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package 
 */
//引用全局
require('glob.php');
//引用顶部栏目
require(DIR_APP_TEMPLATE . DS . 'header.php');
//引用菜单
require(DIR_APP_TEMPLATE . DS . 'menu.php');
//获取用户列表
$chatUserList = $user->viewUserList('1', null, 1, 999);
?>
<div class="container marketing container-fixed">
    <div class="row row-fixed">
        <div class="col-lg-9">
            <h2><span class="glyphicon glyphicon-comment"></span>&nbsp;消息</h2><span class="label label-info" id="messageTip">公共频道</span>
        </div>
        <div class="col-lg-3">
            <h2><span class="glyphicon glyphicon-user"></span>&nbsp;用户列表</h2>
        </div>
    </div>
    <hr>
    <div class="row row-fixed">
        <div class="col-lg-9" id="chatMessage">
            <p>稍等，正在加载聊天数据...</p>
        </div>
        <div class="col-lg-3" id="chatUser">
            <?php if($chatUserList){ foreach($chatUserList as $v){ ?>
            <p data-user-id="<?php echo $v['id']; ?>" data-user-nicename="<?php echo $v['user_nicename']; ?>"<?php if($v['id'] == $userID){ echo ' data-user-self="data-user-self"'; } ?>><?php echo $v['user_nicename']; ?><span> [ 与该人私聊 ] </span></p>
            <?php } } ?>
        </div>
    </div>
    <hr>
    <div class="row row-fixed">
        <div class="col-lg-9">
            <h2><span class="glyphicon glyphicon-bullhorn"></span>&nbsp;发送消息</h2>
            <div class="btn-toolbar" data-role="editor-toolbar"  data-target="#editor">
                <div class="btn-group">
                    <button id="messageFace" class="btn btn-default"  data-container="body" data-toggle="popover" data-placement="bottom"><span class="glyphicon glyphicon-adjust"></span> 表情</button>
                </div>
                <div class="btn-group">
                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-text-height"></span> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a data-edit="fontSize 5"><font size="5">较大字体</font></a></li>
                        <li><a data-edit="fontSize 3"><font size="3">正常字体</font></a></li>
                        <li><a data-edit="fontSize 1"><font size="1">较小字体</font></a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <a class="btn btn-default" data-edit="bold" title="加粗"><span class="glyphicon glyphicon-bold"></span></a>
                    <a class="btn btn-default" data-edit="italic" title="斜体"><span class="glyphicon glyphicon-italic"></span></a>
                    <a class="btn btn-default" data-edit="strikethrough" title="删除线">删除线</a>
                    <a class="btn btn-default" data-edit="underline" title="下划线">下划线</a>
                </div>
                <div class="btn-group">
                    <div style="position:relative;">
                        <a class='btn btn-primary' href='javascript:;'>
                            <span class="glyphicon glyphicon-picture"></span>
                            <input data-edit="insertImage" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                        </a>
                        &nbsp;
                        <span class='label label-info' id="upload-file-info"></span>
                    </div>
                </div>
                <div class="btn-group">
                    <a data-edit="undo" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span></a>
                    <a data-edit="redo" class="btn btn-default"><span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </div>
            <p>
                <div id="editor">请输入内容...</div>
            </p>
            <p>
                <a href="#sendMessage" class="btn btn-success"><span class="glyphicon glyphicon-send"></span> 发送消息</a>
            </p>
        </div>
        <div class="col-lg-3">
            <a href="#clearMessage" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span> 清空消息</a>&nbsp;
            <a href="#ragWhisper" class="btn btn-default"><span class="glyphicon glyphicon-star"></span> 摇一摇</a>&nbsp;
            <a href="#showAll" class="btn btn-default"><span class="glyphicon glyphicon-cloud-download"></span> 查看所有聊天记录</a>&nbsp;
            <a href="#exitWhisper" class="btn btn-default"><span class="glyphicon glyphicon-log-out"></span> 退出私聊</a>
        </div>
    </div>
</div>
<?php
//引用底部
require(DIR_APP_TEMPLATE . DS . 'footer.php');
?>