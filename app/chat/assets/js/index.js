/**
 **
 * 聊天室JS文件
 * @author fotomxq <fotomxq.me>
 * @version 1
 */
//消息自动刷新激活时间变量
var mT;
//刷新时间
var mTs = 3000;
//当前对话人
var sendUserID = 0;
//最后时间段
var lastDateTime = '1990-01-01 0:0:0';
//本次获取的聊天列
var messageList;
//初始化框体标记
var editorStart = true;
//刷新消息列
function refMessage(){
    $.post('action.php?type=message',{
        'time':lastDateTime,
        'send':sendUserID
    },function(data){
        if(data){
            messageList = data;
            lastDateTime = data[data.length-1]['message_time'];
            //刷新列
            for(i=0;i<data.length;i++){
                var sendUserNicename = $('p[data-user-id="'+data[i]['user_id']+'"]').attr('data-user-nicename');
                $('#chatMessage').append('<p>['+data[i]['message_time']+'] '+sendUserNicename+' : </p><p>'+data[i]['message']+'</p><p>&nbsp;</p>');
            }
            //滚动条滚到最下方
            $('#chatMessage').scrollTop(document.getElementById('chatMessage').scrollHeight)
        }
    }, 'json');
    //继续下一次刷新
    clearTimeout(mT);
    var mT = setTimeout('refMessage()',mTs);
}
//清理消息
function clearMessage(){
    $('#chatMessage').html('');
    Messenger().post({message: "清空了所有聊天数据!", type: "info"});
}
//和某人私聊
function whisper(n){
        $('a[href="#exitWhisper"]').fadeIn('fast');
        $('#chatMessage').html('');
        sendUserID = n;
        $('#messageTip').html('与'+$('p[data-user-id="'+n+'"]').attr('data-user-nicename')+'私聊中');
        Messenger().post({message: "进入私聊模式!", type: "info"});
}
//发送消息
function sendMessage(){
    var c = $('#editor').wysiwyg().html();
    if(c){
        $.post('action.php?type=send',{
            'send':sendUserID,
            'message':c
        },function(data){
            if(data == '2'){
                $('#editor').wysiwyg().html('');
                refMessage();
                Messenger().post({message: "发送成功!", type: "success"});
            }else{
                Messenger().post({message: "发送失败!", type: "error"});
            }
        });
    }
}
//插入表情x
function insertFace(n){
    $('#editor').html($('#editor').html()+'<img src="assets/imgs/'+n+'" class="messageFace" />');
}
//初始化
$(function(){
    //初始化富文本编辑器，即消息发送框
    $('#editor').wysiwyg({
        hotKeys: {
            'ctrl+b meta+b': 'bold',
            'ctrl+i meta+i': 'italic',
            'ctrl+u meta+u': 'underline',
            'ctrl+z meta+z': 'undo',
            'ctrl+y meta+y meta+shift+z': 'redo'
        }
    });
    //发送消息
    $('a[href="#sendMessage"]').click(function(){
        sendMessage();
    });
    //清理消息列
    $('a[href="#clearMessage"]').click(function(){
        clearMessage();
    });
    $('#chatMessage').html('');
    //用户列效果
    $('#chatUser > p').hover(function(){
        $('#chatUser > p').css('background-color','');
       $(this).css('background-color','#ccc');
    },function(){
        $('#chatUser > p').css('background-color','');
    });
    //单击用户列动作，进入私聊模式
    $('#chatUser > p').click(function(){
        whisper($(this).attr('data-user-id'));
    });
    //退出私聊模式
    $('a[href="#exitWhisper"]').hide();
    $('a[href="#exitWhisper"]').click(function(){
        sendUserID = 0;
        $('#chatMessage').html('');
        $('#messageTip').html('公共频道');
        $('a[href="#exitWhisper"]').fadeOut('fast');
        Messenger().post({message: "退出私聊模式!", type: "info"});
    });
    //激活表情
    $('#messageFace').popover({
        'html':true,
        'content':'<img class="messageFace" src="assets/imgs/01.png" onClick="insertFace(\'01.png\')" /><img class="messageFace" src="assets/imgs/02.png" onClick="insertFace(\'02.png\')" /><img class="messageFace" src="assets/imgs/03.png" onClick="insertFace(\'03.png\')" /><img class="messageFace" src="assets/imgs/04.png" onClick="insertFace(\'04.png\')" /><img class="messageFace" src="assets/imgs/05.png" onClick="insertFace(\'05.png\')" /><img class="messageFace" src="assets/imgs/06.png" onClick="insertFace(\'06.png\')" /><img class="messageFace" src="assets/imgs/07.png" onClick="insertFace(\'07.png\')" /><img class="messageFace" src="assets/imgs/08.png" onClick="insertFace(\'08.png\')" /><img class="messageFace" src="assets/imgs/09.png" onClick="insertFace(\'09.png\')" />'
    });
    //摇一摇
    $('a[href="#ragWhisper"]').click(function(){
        var rand = parseInt(Math.random()*$('p[data-user-id]').length+1) - 1;
        whisper($('p[data-user-id]').eq(rand).attr('data-user-id'));
    });
    //查看所有聊天记录
    $('a[href="#showAll"]').click(function(){
        $('#chatMessage').html('');
        lastDateTime = '1990-01-01 0:0:0';
        refMessage();
    });
    //初始化框体
    $('#editor').keydown(function(){
        if(editorStart){
            $('#editor').html('');
            editorStart = false;
        }
    });
    //激活自动刷新消息
    refMessage();
});