<?php

/**
 * 
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package 
 */
//引用全局
require('glob.php');

//引用聊天室功能
require('app-chat.php');

//建立聊天室对象
$appChat = new AppChat($db, $appList['chat']['table'][0]);

//动作类型
if(isset($_GET['type']) == true){
    switch($_GET['type']){
        case 'message':
            //获取消息列
            $lastDateTime = isset($_POST['time']) == true ? $_POST['time'] : date('1990-m-d H:i:s');
            $sendUser = isset($_POST['send']) == true ? (int)$_POST['send'] : 0;
            $messageList = $appChat->viewList($lastDateTime,$userID,$sendUser,1,999);
            if($messageList){
                CoreHeader::toJson($messageList);
            }
            break;
        case 'send':
            //发送消息
            if(isset($_POST['message']) == true && isset($_POST['send']) == true){
                //导出并验证输入合法性
                $message = $_POST['message'];
                $sendUser = (int) $_POST['send'];
                //建立消息
                if ($appChat->addMessage($userID, $sendUser, $message) == true) {
                    //建立消息成功
                    die('2');
                }else{
                    die('1');
                }
            }
            break;
        case 'user':
            //获取用户列，这里仅保留该动作，暂时不进行任何操作
            die();
            break;
    }
}
die();
?>