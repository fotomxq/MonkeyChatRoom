<?php

/**
 * 聊天室
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package app-chat
 */
class AppChat{
    /**
     * 数据库对象
     * @var CoreDB 
     */
    private $db;
    /**
     * 数据表名称
     * @var string 
     */
    private $tableName;
    
    /**
     * 字段组
     * @var array 
     */
    private $fields = array('id','user_id','send_user','message','message_time');
    
    /**
     * 初始化
     * @param CoreDB $db 数据库对象
     * @param string $tableName
     */
    public function __construct(&$db,$tableName) {
        $this->db = $db;
        $this->tableName = $tableName;
    }
    
    /**
     * 查看消息列
     * @param string $lastTime 获取时间段之后的内容,eg: 2014-04-23 12:33:25
     * @param int $userID 当前用户ID
     * @param int $sendUser 发送到用户，默认为0则全局
     * @param int $page 页数
     * @param int $max 页长
     * @return array 数据数组
     */
    public function viewList($lastTime,$userID,$sendUser=0,$page=1,$max=10){
        $where = '`'.$this->fields[4].'` > :time';
        $attrs = array(':time'=>array($lastTime,PDO::PARAM_STR));
        if($sendUser > 0){
            $where .= ' and ((`'.$this->fields[2].'` = :send and `'.$this->fields[1].'` = :user) or (`'.$this->fields[1].'` = :send and `'.$this->fields[2].'` = :user))';
            $attrs[':send'] = array($sendUser,PDO::PARAM_INT);
            $attrs[':user'] = array($userID,PDO::PARAM_INT);
        }else{
            $where .= ' and `'.$this->fields[2].'` = 0';
            //$attrs[':user'] = array($userID,PDO::PARAM_INT);
        }
        return $this->db->sqlSelect($this->tableName, $this->fields,$where,$attrs,$page,$max);
    }
    
    /**
     * 添加一条消息
     * @param int $userID 用户ID
     * @param int $sendUser 发送给用户ID，如果全局则为0
     * @param string $message 消息
     * @return int 新消息的ID
     */
    public function addMessage($userID,$sendUser,$message){
        $value = 'NULL,:userID,:send,:message,NOW()';
        $attrs = array(':userID'=>array($userID,PDO::PARAM_INT),':send'=>array($sendUser,PDO::PARAM_INT),':message'=>array($message,PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT));
        return $this->db->sqlInsert($this->tableName, $this->fields, $value, $attrs);
    }
    
    /**
     * 清空聊天记录
     * @return boolean 是否成功
     */
    public function clear(){
        $sql = 'TRUNCATE '.$this->tableName;
        return $this->db->runSQL($sql);
    }
}
?>