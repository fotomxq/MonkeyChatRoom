<?php
/**
 * APP全局设定
 * @author fotomxq <fotomxq.me>
 * @version 1
 * @package web
 */
//引用应用模版全局定义
require(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'template'.DIRECTORY_SEPARATOR.'glob.php');

//设定引用显示名称
$appPage['title'] = '聊天室';

//引用组件
$appPage['glob']['pack'] = array('messenger','icheck','wysiwyg');
$appPage['glob']['css'] = array('icheck-skins-flat','messenger-theme-future');
$appPage['glob']['js'] = array('ftmp-date','menu');
$appPage['temp']['css'] = array('page');

//设定引用脚本和样式
$appPage['js'] = array('index');
$appPage['css'] = array('index');

//设定顶部菜单
$appPage['menu-content-hide'] = true;
$appPage['menu-content'] = array(array('#urlSet','#contentSet'),array('#urlChart','#contentChart'));
?>