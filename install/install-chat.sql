-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014-05-11 11:51:21
-- 服务器版本: 5.6.14
-- PHP 版本: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- 数据库: `yxxlts`
--

-- --------------------------------------------------------

--
-- 表的结构 `chat_message`
--

CREATE TABLE IF NOT EXISTS `chat_message` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '索引',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `send_user` int(11) NOT NULL DEFAULT '0' COMMENT '发送到用户',
  `message` longtext COLLATE utf8_bin NOT NULL COMMENT '聊天内容',
  `message_time` datetime NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;
